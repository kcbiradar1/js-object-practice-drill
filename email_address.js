const data = require('./data');

function extract_email_address(data) {
    const email_address = [];
    if(Array.isArray(data)) {
        for(let index = 0; index < data.length; index++) {
            email_address.push(data[index].email);
        }
    }
    return email_address;
}

const results = extract_email_address(data);

console.log(results);