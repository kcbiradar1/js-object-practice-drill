const data = require('./data');

function ages(data) {
    let age_array = []
    if(Array.isArray(data)) {
        for(let index = 0; index < data.length; index++) {
            age_array.push(data[index].age);
        }
    }
    return age_array;
}

console.log(ages(data));