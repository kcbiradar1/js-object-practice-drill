const data = require('./data');

function city_and_country(data) {
    if(Array.isArray(data)) {
        for(let index = 0; index < data.length; index++) {
            console.log(`${data[index].city} ${data[index].country}`);
        }
    } else {
        console.log(`Error: Can't loop the data`);
    }
}

city_and_country(data);