const data = require('./data');

function hobbies_age_thirty(data,age) {
    if(Array.isArray(data)) {
        let age_thirty_people_hobbies = []
        for(let index = 0; index < data.length; index++) {
            if(data[index].age === age) {
                age_thirty_people_hobbies.push(data[index].hobbies);
            }
        }

        return age_thirty_people_hobbies;
    } else {
        return [];
    }
}

const age_thirty_people_hobbies = hobbies_age_thirty(data , 30);

console.log(age_thirty_people_hobbies);