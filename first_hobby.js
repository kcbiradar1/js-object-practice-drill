const data = require('./data');

function first_hobby(data) {
    let first_hobbies = []
    if(Array.isArray(data)) {
        for(let index = 0; index < data.length; index++) {
            if(data[index]["hobbies"].length > 0) {
                first_hobbies.push(data[index]["hobbies"][0]);
            }
        }
    }

    return first_hobbies
}

console.log(first_hobby(data));