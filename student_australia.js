const data = require('./data');

function student_australia(data) {
    if(Array.isArray(data)) {
        let australia_students = []
        for(let index = 0; index < data.length; index++) {
            if(data[index].country === "Australia" && data[index].isStudent) {
                australia_students.push(data[index].name);
            }
        }
        return australia_students;
    } else{
        return [];
    }
}

const australia_students = student_australia(data);

console.log(australia_students);