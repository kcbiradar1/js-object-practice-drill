const data = require('./data');

function index_data(index) {
    if(data.length < index) {
        return `Can't access the data from ${index}`;
    } else {
        return `${data[index].name}  ${data[index].city}`
    }
}

console.log(index_data(3))