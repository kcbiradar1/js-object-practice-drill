const data = require('./data');

function age_twenty_five(data , age) {
    let count = 0;
    if(Array.isArray(data)) {
        for(let index = 0; index < data.length; index++) {
            if(data[index].age === age) {
                count += 1;
                console.log(data[index].name + " " + data[index].email);
            }
        }
    }
    if(!count) {
        console.log(`There is no one is there with age ${age}`);
    }
}

age_twenty_five(data,25);